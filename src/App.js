import React from 'react';
//import logo from './logo.svg';
import './App.css';
import Game from './components/game/Game'

function App() {
  return (
    <div className="App">
      <Game
        venue="SeaWorld"
        teamOne="Walruses"
        teamOneLogo="./images/Walrus Cartoon_PREVIEW.jpg"
        teamTwo="Seals"
        teamTwoLogo="./images/seal.jpg"
      />
    </div>
  );
}

export default App;
