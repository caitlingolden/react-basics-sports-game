import React, {Component} from "react"
import Team from "../team/Team"

class Game extends Component {
    render() {
      return (
        <div>
          <h1>Welcome to {this.props.venue}!</h1>
  
          <Team name={this.props.teamOne} logo={this.props.teamOneLogo}/>
          <Team name={this.props.teamTwo} logo={this.props.teamTwoLogo}/>
        </div>
      );
    }
  }

  export default Game