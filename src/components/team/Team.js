import React from "react"

class Team extends React.Component {

    state = {
      shotsTaken: 0,
      score: 0,
      mode: 0
    };
    soundEffects = {
      shooting: "./sounds/BOUNCE+1.mp3",
      scoring: "./sounds/Swish+2.mp3",
    };
  
    shots = () => {
      new Audio(this.soundEffects.shooting).play();
      this.setState((state) => {
        console.log("Shots Works");
        return { shotsTaken: state.shotsTaken + 1 };
      });
  
      let shot = Math.round(Math.random() * 100);
      if (shot % 3 === 0) {
        new Audio(this.soundEffects.scoring).play();
        console.log("Scoring Works...");
        this.setState((state) => {
          return { score: state.score + 1 };
        });
      }
    };
  
    percentage = () => {
      if (this.state.shotsTaken > 0) {
        console.log("Percentage Should Update");
        return (
          "Percentage:" +
          Math.round((this.state.score / this.state.shotsTaken) * 100) +
          "%"
        );
      }
      console.log("No Shots Attempted");
      return;
    };
  
    render() {
      return (
        <div>
          <React.Fragment>
            <h2> {this.props.name}</h2>
            <img src={this.props.logo} alt="logo" />
            <button onClick={this.shots}>Take a shot!</button>
            <h4>Shots Taken: {this.state.shotsTaken}</h4>
            <h4>score: {this.state.score}</h4>
            {<h4>{this.percentage()}</h4>}
          </React.Fragment>
        </div>
      );
    }
  }
  export default Team